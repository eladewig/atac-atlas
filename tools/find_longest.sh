#!/bin/bash

declare -A maxmap
declare -A ctname

# test for associative array
#maxmap["CD8_1"]=272
#maxmap["CD8_2"]=223

#if (( ${maxmap[CD8_1]} > ${maxmap[CD8_2]} )); then
#    echo "CD8_1 has more"
#else
#    echo "CD8_2 has more (it does not.)"
#fi

# intialize maxes to zero
for peaks in $(find . -name *IDR_ranked_peaks.broadPeak)
do
    celltype=$(basename $(dirname $peaks))
    maxmap[$celltype]=0
done 

# find the maximum number of peaks in each comparison

for peaks in $(find . -name *IDR_ranked_peaks.broadPeak)
do
     goodpeaks=$(cat $peaks | awk '$5 >= 1000' | wc -l)
     celltype=$(basename $(dirname $peaks))
     filename=$(basename $peaks)

     if (( $goodpeaks > ${maxmap[$celltype]} )); then
	maxmap[$celltype]=$goodpeaks
	ctname[$celltype]=$filename
     fi     
done

# print the tables
for sound in "${!animals[@]}"; do echo "$sound - ${animals[$sound]}"; done

for ct in "${!maxmap[@]}"; do
     echo "$ct  -  ${maxmap[$ct]}  - ${ctname[$ct]}"
done
