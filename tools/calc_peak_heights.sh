#!/bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="calc_peak_heights.sh -- for a given celltype, count the summit heights per replicate for each peak 
Usage:
   bash calc_peak_heights.sh [-h] -b base dir -p peaks root dir -f scaling_factors -g celltype

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -p  path to output base directory for peaks (e.g 'data/peaks' means the output will end up in base/data/peaks)
    -f  path to scaling factors file, relative to the base directory (e.g 'results/myfile.csv' is the file in base/results/myfile.csv)
    -g  subdirectory of bams/peaks directories that is associated with a given celltype.
"
declare option
declare OPTARG
declare base
declare peaks
declare factorfile
declare -A sfmap
declare celltype

while getopts b:p:g:f:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    p) peaks=$OPTARG
       ;;
    g) celltype=$OPTARG
	   ;;
    f) factorfile=$OPTARG
     ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

peaks_root="$base/$peaks"
scripts_dir=`pwd`

myfactorfile="$base/$factorfile"
[[ -f "$myfactorfile" ]] || { echo "Error: $myfactorfile is not a size factor file"; exit 1; }


## get the reproducible peaks for this celltype
echo "cd to $peaks_root/$celltype..."

cd $peaks_root/$celltype
peak_file=$(echo $celltype"_reproducible_peaks.bed")
[[ -f "$peak_file" ]] || { echo "Error: $peak_file is not a cell specific peak bedfile"; exit 1; }

## use bedtools to get the max of the reads under the region for each replicate
treat_pileups=$(find . -name *treat_pileup.bdg)

sort_bedgraphs(){
  infile=$1
  outfile=$(echo $infile | sed -e 's/pileup.bdg/pileup_sorted.bdg/')
  sort -k1,1 -k2,2n -k3,3n $infile > $outfile
}

export -f sort_bedgraphs

echo "sorting treat pileup bedgraphs for $celltype..."
num_treatments=$(find . -name *treat_pileup.bdg | wc -l)
num_sorted=$(find . -name *pileup_sorted.bdg | wc -l)
if [[ ($num_treatments -gt 0) && ($num_sorted -eq $num_treatments) ]]; then
  echo "found $num_treatments bedfiles and $num_sorted sorted bedfiles "
else
  echo "found $num_treatments bedgraphs and $num_sorted sorted bedgraphs"
  parallel --gnu -j 6 --progress --xapply sort_bedgraphs ::: $treat_pileups
fi

echo "done..."

find_summit_heights(){
  celltype_peaks=$1
  peak_bdg=$2
  outfile=$(echo $peak_bdg | sed -e 's/_treat_pileup_sorted.bdg/_peak_summits.bed/')
  bedtools map -a $celltype_peaks -b $peak_bdg -c 4 -o max > $outfile
}

export -f find_summit_heights

echo "finding summit heights per celltype..."
treat_pileups_sorted=$(find . -name *treat_pileup_sorted.bdg)
## compute intersections
parallel --gnu -j 6 --progress --xapply find_summit_heights ::: $peak_file ::: $treat_pileups_sorted
echo "done..."

## read in scaling factors from file
oldIFS=${IFS}
IFS=","

while read -r -a array
do
  sfmap["${array[0]}"]="${array[2]}"
done < $myfactorfile
IFS=${oldIFS}

echo "got scaling factors..."

## multiply each peak height for each rep by the appropriate scaling factor using awk
echo "scaling peak summits by scaling factors..."
for summits in $(find . -name "*_peak_summits.bed")
do
  # get the name of the rep
  repname=$(basename $summits | sed -e 's/_peak_summits.bed//')
  outfilename=$(echo $summits | sed -e 's/summits.bed/summits_normalized.bed/')

  # get the sizeFactor from the map
  mysf=${sfmap["$repname"]}
  echo "${repname}  ---> $mysf"

  # read each line, multiply, write out
  cat $summits | awk -v SF="$mysf" '{printf($1"\t"$2"\t"$3"\t"$4*SF"\n")}' > $outfilename
done
echo "done..."

## sort && merge the files together, group by peak and collect the summit heights, write out
outfile=$(echo $celltype"_normalized_summit_heights.bed")
echo "grouping normalized summit files by peak, take median, write to file..."
sort -k1,1V -k2,2n MACS2/*summits_normalized.bed | bedtools groupby -g 1,2,3 -c 4 -o median > $outfile

echo "done"



