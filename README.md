ATAC-seq pipeline
=================

The Leslie lab's standard ATAC-seq pipeline begins with fastq files as input, and produces a BEDfile output consisting of the regions of Tn5 accessible chromatin as output.  Intermediate outputs include bedgraph files for celltype specific (and replicate specific) coverage tracks, MACS2 output, and QC for pre-aligned read 
data via fastqc (best summarized with multiqc).  It also supports grabbing fastq files from an .sra file.  As a flow chart, the pipeline is rendered as follows:

![atac-seq pipeline flowchart](./logo.svg "ATAC-seq pipeline flow chart")

It is runnable either on a single machine, or as a set of sequential jobs to be run on an HPC system.

### How to use this repo

1. [Fork the repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html#ForkingaRepository-HowtoForkaRepository) to your own account 
2. [Clone the forked](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) repository to your workstation / cluster account
3. Make any necessary changes to the job submit scripts so it points to your data

### Directory Contents

- `pipeline`: the bash scripts that contain each step of the pipeline
- `submit_scripts`: the LSF submit scripts for each step, plus bash scripts to process entire 
- `omitted_regions`: contains bed files which catalog regions which show high degrees of accessibility regardless of experimental condition or mappability (centromeres, telomeres, etc.) and are therefore usually filtered out mid-pipeline.  Sourced from the [Kundaje lab](https://sites.google.com/site/anshulkundaje/projects/blacklists)
- `tools`: contains scripts that are useful but not part of the core pipeline

It can be run either in a single cell types (possibly with multiple timepoints), or on multiple celltypes with multiple replicates.

### *** READ THIS *** Expected project structure

This version of the pipeline expects your data to be laid out in a directory structure as follows:

* `data/`                 (the root directory for all your data)
   * `fastq/`             (where your fastq files live)
   * `bam/`               (where your bam files live)
   * `peaks/`             (where your MACS peaks will live)
   * `results/`           (the atlas will appear here)


Each of the subdirectories under `data/` should contain either celltype (or timepoint) specific folders.  It is enough to do this manually for `fastq/`,  the subsequent scripts should create the rest for you in each of `bam/`, and `peaks/`.
 
### Running the pipeline 

The files are named in a way that suggests their order.  

1. Run `bash check_dependencies.sh` to check that you have the dependencies installed
2. Run `fastq_to_bam.sh` to align your reads, and generate sorted bam files
3. Run `bams_to_peaks.sh` to call peaks in each celltype and replicate 
4. Run `peaks_to_candidates.sh` to convert peaks called on individual timepoint/celltypes into set of common peak regions
5. Run `candidates_to_celltypes.sh` to convert candidate peaks called on a merged bam into IDR validated peaks for each celltype
6. Run `celltypes_to_atlas.sh` to merge the tournament broadPeak files into celltype specific bed files, and a unified atlas bed

Each script has help documentation that describes how to set each of the command line flags for every step.  Below is an example for the `bam_to_peaks` script.

```
$:pipeline zamparol$ bash bam_to_peaks.sh -h
bam_to_peaks.sh -- pipeline to convert bam files measured across different timepoints to IDR validated peaks
Usage:
   bash bam_to_peaks.sh [-h] -b base dir -d data root dir -p put peaks here -g celltype

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -a  path extension below base directory where the bams data resides (e.g 'data/bam' means the data is in base/data/bam)
    -p  path to output base directory for peaks (e.g data/peaks means the output will end up in base/data/peaks)
    -g  sub-directory that is associated with a given celltype (below where all bam files live)

```

### Subsequent (optional) steps

- Annotate your peak atlas with AtlasAnnotator (R package available from LeslieLab bitbucket).  

### Any questions?

Contact Lee (zamparol@cbio)


