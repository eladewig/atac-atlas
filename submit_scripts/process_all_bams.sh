#! /bin/bash

# grab all cell types, submit the bam -> peaks conversion jobs
all_cell_types=$(ls -1 /data/leslie/zamparol/heme_ATAC/data/bam)

for f in $all_cell_types;
do
   #qsub -v CT=$f submit_bam_to_peaks.sh
   bsub -env "all, CT=$f" < submit_bam_to_peaks.lsf
   sleep 1
done
