#! /bin/bash

# make up a genes bed file
# here's an example for human.  For mouse you need a different URL for the gff3 file.  Requires gff2bed
wget ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_21/gencode.v21.annotation.gff3.gz
gunzip gencode.v21.annotation.gff3.gz
gff2bed < gencode.v21.annotation.gff3 > gencode_21_annotations.bed



# simplify the atlas annotations
python annotate_atlas.py /Users/zamparol/projects/thymus/data/peaks/multi_annotated_peak_atlas.bed  /Users/zamparol/projects/thymus/data/peaks/single_annotated_peak_atlas.bed

# associate each peak its nearest gene
bedtools closest -a ../peaks/all_timepts_peak_atlas.bed -b mm10_genes_sorted.bed -d -t first > peaks_to_closest_gene.bed