#!/bin/bash

### check for tools needed to run the pipeline
biostarurl='https://www.biostarhandbook.com/tools/how-to-install-everything.html'

### horrifying awk function to strip version numbers.
function version { echo "$@" | awk -F. '{ printf("%d%03d%03d\n", $1,$2,$3); }'; }


# check for fastqc
echo "   Checking for fastqc"
hash fastqc 2>/dev/null || { echo >&2 "*** fastqc is required but it's not installed.  Follow the instructions at $biostarurl.  Also, 'conda install -c bioconda fastqc' should work."; exit 1; }

# check for bowtie2
echo "   Checking for bowtie2"
hash bowtie2 2>/dev/null || { echo >&2 "*** bowtie2 is required but it's not installed.  Follow the instructions at $biostarurl.   Also, 'conda install -c bioconda bowtie2' should work."; exit 1; }

# check for trimmatic
echo "   Checking for trimmomatic"
hash trimmomatic 2>/dev/null || { echo >&2 "*** trimmomatic is required but it's not installed.  Follow the instructions at $biostarurl.  Also, 'conda install -c bioconda trimmomatic' should work."; exit 1; }

# check for samtools
echo "   Checking for samtools"
hash samtools 2>/dev/null || { echo >&2 "*** samtools is required but it's not installed.  Follow the instructions at $biostarurl"; exit 1; }

# check for bedtools
echo "   Checking for bedtools"
hash bedtools 2>/dev/null || { echo >&2 "*** bedtools is required but it's not installed.  Follow the instructions at $biostarurl"; exit 1; }

# check for parallel
echo "   Checking for GNU parallel"
hash parallel 2>/dev/null || { echo >&2 "*** GNU parallel is required but it's not installed.  'conda install -c bioconda parallel' should work."; exit 1; }

# check for macs2
echo "	Checking for macs2"

# check for idr
echo "   Checking for idr"
hash idr 2>/dev/null || { echo >&2 "*** idr is required but it's not installed.  Follow the instructions at https://github.com/nboley/idr.  Also, 'conda install -c bioconda idr' should work."; exit 1; }


echo "Ok, everything looks good."
