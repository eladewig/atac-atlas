#! /bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit


### parse arguments:

usage="fastq_to_bam.sh -- pipeline to convert fastq files measured across different timepoints to sorted bam files
Usage:
   bash fastq_to_bam.sh [-h] -b base dir -i path to bowtie2 index -d data root dir -g group dir -l left mate substring -r right mate substring 

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -i  path to directory containing bowtie2 index files. (e.g ~/genomes/mm10/chromFa/index)
    -f  path extension below base directory where the fastq files reside (e.g 'data/fastq' means the data is in base/data/fastq)
    -g  group directory.  All fastq files below this will be processed together (for clusters)
    -l  substring in fasta files that identifies the left mate substring (e.g R1)
    -r  substring in fasta files that identifies the right mate substring (e.g R2)
"
declare option
declare OPTARG
declare base
declare index
declare data
declare left
declare right
declare myfastq

while getopts hb:i:f:l:r:g: option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    i) index=$OPTARG
       ;;
    f) data=$OPTARG
       ;;
    l) left=$OPTARG
       ;;
    r) right=$OPTARG
       ;;
    g) myfastq=$OPTARG
	   ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

data_root="$base/$data"
bams_root="$(dirname $data_root)/bam"
adapters="/cbio/cllab/home/zamparol/anaconda3/pkgs/trimmomatic-0.36-3/share/trimmomatic-0.36-3/adapters/"

### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }
[[ -d "$data_root" ]] || { echo "Error: $data_root is not a directory"; exit 1; }
[[ -n "$left" ]] || { echo "Error: you must indicate how to identify the left mate reads with -l"; exit 1; }
[[ -n "$right" ]] || { echo "Error: you must indicate how to identify the right mate reads with -r"; exit 1; }


echo "starting pipeline..."

### debugging info
echo "   using $index for index dir"
echo "   using $data_root for data root dir"
echo "   using $bams_root for bams root dir"
echo "   using $left for left mate substring"
echo "   using $right for right mate substring"

### validate that there exists index files in $index, get the common prefix
count=$(ls -1 $index/*.bt2 2>/dev/null | wc -l)
if [[ $count != 6 ]]; then
  	echo "did not detect any bowtie2 index files in $index!"
  	exit 1
fi
echo "   detected a valid index in $index..."


### fastqc results will appear here
fastqc_results_root="$(dirname $data_root)/results/fastqc"


### fastqc tests
if [ -z ${myfastq+x} ]; then
	echo "run fastqc tests?"
	select do_fastqc in "y" "n";
	do
		if [ "$do_fastqc" == "y" ]; then

			for timept in `ls -1 $data_root`
			do
				if [ ! -d "$fastqc_results_root/$timept" ]; then
  					mkdir -p "$fastqc_results_root/$timept"
				fi
				outdir_fastq_timept="$fastqc_results_root/$timept"
			    cd "$data_root/$timept"
			    echo "processing $timept files..."
			    ls -1 *.gz | parallel --gnu --progress fastqc -q -o $outdir_fastq_timept
			done

		fi
		break;
	done
else
	echo "running fastqc tests..."
	mydir="$data_root/$myfastq"
	celltype=$(basename $mydir)
	if [ ! -d "$fastqc_results_root/$celltype" ]; then
  		mkdir -p "$fastqc_results_root/$celltype"
	fi

	outdir_fastq_timept="$fastqc_results_root/$celltype"
	cd $mydir
	fastqc -q --outdir $outdir_fastq_timept *.gz
fi

trim_reads(){
	lr=$1
	rr=$2
	left_read=$3
	right_read=$4
	adapter_dir=$5
	trl=$(echo $1 | sed -e "s/$left_read/${left_read}.trimmed/g")
	trr=$(echo $2 | sed -e "s/$right_read/${right_read}.trimmed/g")
	unl=$(echo $1 | sed -e "s/$left_read/${left_read}.Unpaired/g")
	unr=$(echo $2 | sed -e "s/$right_read/${right_read}.Unpaired/g")

	trimmomatic PE \
		"$lr" \
		"$rr" \
		"$trl" \
		"$unl" \
		"$trr" \
		"$unr" \
		ILLUMINACLIP:"$adapter_dir"/NexteraPE-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:25
}

export -f trim_reads

### trim reads?
if [ -z ${myfastq+x} ]; then
	echo "trim all read with Trimmomatic?"
	select do_trim in "y" "n";
	do
		if [ "$do_trim" == "y" ]; then

			adapters="$base/tools/Trimmomatic-0.36/adapters"
			declare -a timepts=(`ls -1 $data_root/fastq`)
			for timept in "${timepts[@]}"
			do
				F1=( $data_root/fastq/$timept/*$left*.fastq.gz )
	    		F2=( $data_root/fastq/$timept/*$right*.fastq.gz )
	    		echo "found ${#F1[@]} left ends"
	    		echo "found ${#F2[@]} right ends"

	    		trimmomatic PE \
	        	$F1 \
	        	$F2 \
	        	${F1/\.R1\./.R1.trimmed.} \
	        	${F1/\.R1\./.R1.Unpaired.trimmed.} \
	        	${F2/\.R2\./.R2.trimmed.} \
	        	${F2/\.R2\./.R2.Unpaired.trimmed.} \
	        	ILLUMINACLIP:$adapters/TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
			done
		fi
		break;
	done
else
	echo "trimming PE reads..."
	mydir="$data_root/$myfastq"
	cd $mydir

	lefts=( $(ls *$left*.fastq.gz) )
	rights=( $(ls *$right*.fastq.gz) )
	echo "found ${#lefts[@]} left ends"
	echo "found ${#rights[@]} right ends"

	for i in "${!lefts[@]}";
	do
    	trim_reads "${lefts[$i]}" "${rights[$i]}" "$left" "$right" "$adapters"
	done

fi

### TODO: broken, need to remake directory walk 
### check test output for failed tests
check_fail(){
	unzip -q $1
	filename=$(basename "$1")
	prefix="${filename%.*}"
	fails=`cat $prefix/summary.txt | grep -c FAIL`
	echo $fails
}

cleanup_dirs(){
	filename=$(basename "$1")
        prefix="${filename%.*}"
	if [ -d "$prefix" ]; then
		rm -rf "$prefix"
	fi
}

export -f check_fail
export -f cleanup_dirs

### analyze fastqc results?  N.B probably not needed, MultiQC can do this better...
if [ -z ${myfastq+x} ]; then
	echo "analyze fastqc results?"
	select analyze in "y" "n";
	do
		if [ "$analyze" == "n" ]; then
			break;
		elif [ "$analyze" == "y" ]
		then
			cd "$outdir_rep1"
			num_files=`ls -1 *.zip | wc -l`
			echo "found $num_files files in rep1..."
			echo "unzipping, counting failures"
			fails=`ls -1 *.zip | parallel --gnu --progress check_fail | paste -sd+ | bc`
			echo "found $fails failures in rep 1"

			cd "$outdir_rep2"
			num_files=`ls -1 *.zip | wc -l`
			echo "found $num_files files in rep2..."
			echo "unzipping, counting failures"
			fails=`ls -1 *.zip | parallel --gnu --progress check_fail | paste -sd+ | bc`
			echo "found $fails failures in rep 2"
			break;
		fi
	done

	echo "clean up directories?"
	select cleanup in "y" "n";
	do
		if [ "$cleanup" == "y" ]; then
			cd $outdir_rep1
			ls -1 *.zip | parallel --gnu cleanup_dirs
			cd $outdir_rep2
			ls -1 *.zip | parallel --gnu cleanup_dirs
		fi
		break;
	done
else
	echo "N.B: do not forget to analyze fastqc results using MultiQC..."
fi


do_merges(){
	prefix=$1
	cat "$prefix"*R1*trimmed.fastq.gz 1> ./MERGED/"$prefix"_R1.fastq.gz
    cat "$prefix"*R2*.trimmed.fastq.gz 1> ./MERGED/"$prefix"_R2.fastq.gz
}

export -f do_merges

### merge technical replicates
if [ -z ${myfastq+x} ]; then
	echo "merge technical replcates?"
	select do_merge_reps in "y" "n";
	do
		if [ "$do_merge_reps" == "y" ]; then

			for timept in `ls -1 $data_root`
			do
				cd "$data_root/$timept"
				echo "in $(pwd)"
				mkdir -p MERGED
				files=$(ls -1 *fastq.gz)
				prefixes=$(ls -1 *.gz | cut -d'_' -f1 | cut -d'-' -f1 | uniq) # thymus proj: $(ls -1 *.gz | cut -d'_' -f1 | uniq)
				echo "found these prefixes: $prefixes"
				parallel --gnu -j2 --progress --xapply do_merges ::: $prefixes
			done

		fi
		break;
	done
else
	echo "merging technical replicates"
	mydir="$data_root/$myfastq"
	cd $mydir
	mkdir -p MERGED
	files=$(ls -1 *trimmed.fastq.gz)
	prefixes=$(ls -1 *.gz | cut -d'_' -f1 | cut -d'-' -f1 | uniq)
	echo "found these prefixes: $prefixes"
	parallel --gnu -j8 --progress --xapply do_merges ::: $prefixes
fi


### outfile removes endedness (R1/R2), and replaces suffix of fastq with sam
do_align(){
	outfile=`echo $1 | sed -e 's/_R1//g' -e 's/.fastq.gz/.sam/g'` # thymus proj: .R1 
	#bwa mem -t 4 genome.fa $1 $2 > $outfile
	index_prefix=$(ls -1 *.bt2 | head -n 1 | sed -e 's/.[0-9].bt2//g' -e 's/.rev//g')
	bowtie2 -q --no-mixed --no-discordant -p 8 --met-file metrics.txt -x $index_prefix -1 $1 -2 $2 -S $outfile 2>> align.err
	
}
export -f do_align

### make links to genome index files
### handle the case where $ref is just the filename, as well as when it is a path
make_links(){
	ref=$(ls -1S $(dirname $index)/*.fa | head -n 1)
	if [ -f "$ref" ]; then
  		ln -s $ref $(basename $ref)
	else
		ln -s "$index/$ref" "$ref"
	fi
	
	for file in $(ls -1 $index/*.bt2)
    do
        ln -s $file $(basename $file)
    done

}

### remove links
clean_up_links(){
	fas=$(ls -1 | grep .fa)
	if [ ! -z ${fas+x} ]; then
		rm *.fa
	fi
	bts=$(ls -1 | grep .bt2)
	if [ ! -z ${bts+x} ]; then
		rm *.bt2
	fi
}

export make_links
export clean_up_links

### align the reads
if [ -z ${myfastq+x} ]; then
	echo "align reads with bowtie2?"
	select align in "y" "n";
	do
		if [ "$align" == "y" ]; then

			for timept in `ls -1 $data_root`
			do
			    cd "$data_root/$timept/MERGED"
			    echo "processing merged $timept .fastq files..."
			    make_links
			    lefts=`ls -1 *$left*.fastq.gz`
			    rights=`ls -1 *$right*.fastq.gz`
			    parallel --gnu -j1 --progress --xapply do_align ::: $lefts ::: $rights
			    clean_up_links
			done

		fi
		break;
	done
else
	echo " aligning with bowtie2..."
	# mydir="$data_root/$myfastq"
	# cd "$mydir/MERGED"
	# make_links
 #    lefts=`ls -1 *$left*.fastq.gz`
 #    rights=`ls -1 *$right*.fastq.gz`
 #    parallel --gnu -j8 --progress --xapply do_align ::: $lefts ::: $rights
 #    clean_up_links
fi

### convert sam to sorted bam files, filtering the poor quality reads

sam_to_bam(){
	outfile=$(echo $2 | sed -e 's/.sam/.bam/g')
	echo "using outfile : $outfile"
	samtools view -bSq $1 $2 > $outfile 
}

export -f sam_to_bam

if [ -z ${myfastq+x} ]; then
	echo "convert sam to bam?"
	select convert in "y" "n";
	do
		if [ "$convert" == "y" ]; then

			qval=33
			echo "Using phred probability quality cutoff value $qval"

			for timept in `ls -1 $data_root`
			do
			    cd "$data_root/$timept/MERGED"
			    echo "processing $timept .sam files..."
			    parallel --gnu -j8 --progress --xapply sam_to_bam ::: $qval ::: `ls -1 *.sam`
			    mv *.bam "$bams_root/$timept"
			done

		fi
		break;
	done
else
	qval=33
	echo "Using phred probability quality cutoff value $qval"
	mydir="$data_root/$myfastq"
	cd "$mydir/MERGED"
	parallel --gnu -j8 --progress --xapply sam_to_bam ::: $qval ::: `ls -1 *.sam`
	celltype=$(basename $mydir)
	if [ ! -d "$bams_root/$celltype" ]; then
  		mkdir -p "$bams_root/$celltype"
	fi
	mv *.bam "$bams_root/$celltype"
fi

if [ -z ${myfastq+x} ]; then
	echo "delete sam files?"
	select rm_sam in "y" "n";
	do
		if [ "$rm_sam" == "y" ]; then

			cd "$data_root"
			find . -name *.sam -delete

		fi
		break;
	done	
else
	echo "delete sam files manually..."
fi

sort_bam(){
	outfile=`echo $1 | sed -e 's/.bam/_sorted.bam/g'`
	samtools sort $1 -o $outfile
}
export -f sort_bam

if [ -z ${myfastq+x} ]; then
	echo "Sort bam files?"
	select dosort in "y" "n";
	do
		if [ "$dosort" == "y" ]; then
			
			for timept in `ls -1 $bams_root`
			do
			   cd "$bams_root/$timept"
			   ls -1 *.bam | parallel --gnu -j8 --progress sort_bam
			done

		fi
		break;
	done
else
	echo "Sorting bam files..."
	mydir="$bams_root/$myfastq"
	cd $mydir
	bams=$(ls -1 *.bam)
	echo "found $bams"
	parallel --gnu -j8 --progress --xapply sort_bam ::: $bams
fi

if [ -z ${myfastq+x} ]; then
	echo "Remove unsorted bam files?"
	select rm_bam in "y" "n";
	do
		if [ "$rm_bam" == "y" ]; then
			
			cd "$bams_root"
			find . -name *[0-9].bam -delete

		fi
		break;
	done	
else
	echo "Remove unsorted bam files manually..."
fi

if [ -z ${myfastq+x} ]; then
	echo "Index sorted bam files?"
	select do_index in "y" "n";
	do
		if [ "$do_index" == "y" ]; then

			for timept in `ls -1 $bams_root`
			do
			   cd "$bams_root/$timept"
			   ls -1 *_sorted.bam | parallel --gnu -j8 --progress samtools index 
			done
			

		fi
		break;
	done
else
	echo "Indexing sorted bam files..."
	mydir="$bams_root/$myfastq"
	cd $mydir
	sorted_bams=$(ls -1 *_sorted.bam)
	parallel --gnu -j8 --progress --xapply samtools index ::: $sorted_bams
fi

